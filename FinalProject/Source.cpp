﻿#include<windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <strsafe.h>

#include"MyDialogResource.h"
#include "SecondDialogHeader.h"
#include"ImageHeader.h"

LRESULT CALLBACK WndProc(HWND ,UINT,WPARAM,LPARAM);
BOOL CALLBACK MyDlgProc(HWND,UINT,WPARAM,LPARAM);
BOOL CALLBACK MyDlgProc2(HWND,UINT,WPARAM,LPARAM);
BOOL CALLBACK MyDlgProc3(HWND,UINT,WPARAM,LPARAM);

TCHAR Name[50];

struct Cost
{
	int Cpu;
	TCHAR CPUText[20];

	int Ram;
	TCHAR RAMText[20];

	int MotherBoard;
	TCHAR MotherBoardText[20];

	int GraphicsCard;
	TCHAR GraphicsCardText[20];

	int HardDisk;
	TCHAR HardDiskText[20];

	int CdDvdDrive;
	TCHAR CdDvdDriveText[20];

	int Smps; 
	TCHAR SmpsText[20];

	int  Cabinate;
	TCHAR CabinateText[20];

	int Monitor;
	TCHAR MonitorText[20];

	int Keyboard;
	TCHAR KeyboardText[20];

	int Mouse;
	TCHAR MouseText[20];

	int Total;
	TCHAR TotalCostText[20];

}Cost;


struct MYINPUT
{
	TCHAR CpuType[50];
	TCHAR CpuGen[50];
	TCHAR CpuArch[50];

	TCHAR RamCompany[50];
	TCHAR RamSize[50];

	TCHAR MotherBoardCompany[50];
	TCHAR MotherBoardType[50];

	TCHAR GraphicsCardCompany[50];
	TCHAR GraphicsCardType[50];

	TCHAR HardDiskCompany[50];
	
	TCHAR CdDvdCompany[50];
	TCHAR cdDvdType[50];

	TCHAR SmpsCompany[50];
	TCHAR SmpsType[50];

	TCHAR CabinateCompany[50];
	TCHAR CabinateType[50];

	TCHAR KeyboardCompany[50];
	TCHAR KeyBoardType1[50];
	TCHAR KeyboardType2[50];

	TCHAR MouseCompany[50];
	TCHAR MouseType1[50];
	TCHAR MouseType2[50];

	TCHAR MonitorCompany[50];
	
}in;
HBITMAP hBitmap = NULL;

TCHAR *CPU[] = { TEXT("****Select Company****") ,TEXT("AMD"),TEXT("INTEL"),NULL};

//Inetl
TCHAR *CPUGENINTEL[] = {TEXT("****Selelct Generation****"),TEXT("i3"),TEXT("i5"),TEXT("i7"),NULL};
TCHAR *CPUARCHINTEL[] = {TEXT("****Select Architechture****"),TEXT("Haswell"),TEXT("Skylake"),TEXT("Broadwell"),NULL};
//AMD
TCHAR *CPUGENAMD[] = { TEXT("****Selelct Generation****"),TEXT("AMD FX"),TEXT("AMD A4"),TEXT("AMD A8"),NULL };
TCHAR *CPUARCHAMD[] = { TEXT("****Selelct Architechture****"),TEXT("Optron"),TEXT("Athlon"),TEXT("Sampron"),NULL };



//RAM section
TCHAR *RAMCOM[] = {TEXT("**select Company**"),TEXT("Corsair"),TEXT("KingSton"),TEXT("G.SKILL"),NULL};
TCHAR *RAMSIZE[] = { TEXT("**Select Size**"),TEXT("4GB"),TEXT("8GB"),TEXT("16GB"),NULL };


//motherboard section
TCHAR *MOTHERBOARDCOM[] = {TEXT("**Select Company**"),TEXT("Asus"),TEXT("Biostar"),TEXT("GigaByte"),NULL};
TCHAR *MOTHERBOARDASUS[] = { TEXT("**Select Model**"),TEXT("Asus H270+"),TEXT("Asus Intel H61M"),NULL };
TCHAR *MOTHERBOARDGIGABYTE[] = { TEXT("**Select Model**"),TEXT("GA-H110M-S2 LGA"),TEXT(" GA-H61M-S*"),NULL };
TCHAR *MOTHERBOARDBIOSTAR[] = { TEXT("**Select Model**"),TEXT("TB250-BTC"),TEXT("J3160NH Mini ITX"),NULL };


//Graphics card
TCHAR *GRAPHICSCARDCOM[] = {TEXT("**Select Company**"),TEXT("AMD"),TEXT("NVIDIA"),NULL};
TCHAR *GRAPHICSCARDNVDIA[] = { TEXT("**Select Type**"),TEXT("Quedro K620"),TEXT("NV5315"),NULL };
TCHAR *GRAPHICSCARDAMD[] = { TEXT("**Select Type**"),TEXT("A10 7990K "),TEXT("Radeon ProWX3100"),NULL };


//HardDisk

TCHAR *HARDDISK[] = {TEXT("**Select Company**"),TEXT("Seagate"),TEXT("Western Digital"),TEXT("TOSHIBA"),NULL};

//CD/DVD drive
TCHAR *CDDVDDRIVECOM[] = {TEXT("**Select Company**"),TEXT("Dell"),TEXT(" HP "),TEXT("Asus"),NULL};
TCHAR *CDDVDDELL[] = {TEXT("**Select Type **"),TEXT("Dell DW316"),TEXT("DELL Original 16X DVD"),NULL};

TCHAR *CDDVDHP[] = { TEXT("**Select Type **"),TEXT("HP F6V97AA"),TEXT("HP external USB"),NULL };

TCHAR *CDDVDASUS[] = { TEXT("**Select Type **"),TEXT("Asus Internal DVD Writer DRW"),TEXT("Asus Internal DVD Writer "),NULL };

//SMPS
TCHAR *SMPSCOM[] = {TEXT("**Select Company**"),TEXT("Corsair "),TEXT("Dell "),NULL};
TCHAR *SMPSCORSAIR[] = { TEXT("**Select Type**"),TEXT("Corsair VS550  "),TEXT("Corsair VS350"),NULL };
TCHAR *SMPSDELL[] = { TEXT("**Select Type**"),TEXT("Dell Optiplex  "),TEXT("Dell OEM 250W"),NULL };


//Cabinate

TCHAR *CABINATECOM[] = {TEXT("**Select Company**"),TEXT("Dell"),TEXT(" Corsair "),NULL};
TCHAR *CABINATEDELL[] = { TEXT("**Select Type**"),TEXT("Dell XPS"),TEXT("Dell Optiplex"),NULL };
TCHAR *CABINATECORSAIR[] = { TEXT("**Select Type**"),TEXT("Corsair Carbide"),TEXT("Corsair CC-9011052"),NULL };



TCHAR *KEYBOARDCOM[] = {TEXT("**Select Company**"),TEXT("Dell"),TEXT("Logitech"),NULL};
TCHAR *KEYBOARDTYPE[] = { TEXT("**Select Type**"),TEXT("Wired"),TEXT("Wireless"),NULL };

TCHAR *KEYBOARDDELLWired[] = {TEXT("**Select Type**"),TEXT("Dell KB216"),TEXT("Dell KB212"),NULL};
TCHAR *KEYBOARDDELLWireLess[] = { TEXT("**Select Type**"),TEXT("Dell KM117"),TEXT("Dell KM632"),NULL };

TCHAR *KEYBOARDLOGITECHWired[] = { TEXT("**Select Type**"),TEXT("Logitech K120"),TEXT("Logitech Prodigy G213"),NULL };
TCHAR *KEYBOARDLOGITECHWireLess[] = { TEXT("**Select Type**"),TEXT("Logitech K230"),TEXT("Logitech MK215"),NULL };


TCHAR *MOUSECOM[] = {TEXT("**Select Company**"),TEXT("Dell"),TEXT(" Logitech "),NULL};
TCHAR *MOUSETYPE[] = { TEXT("**Select Type**"),TEXT("Wired"),TEXT("Wireless"),NULL };

TCHAR *MOUSEDELLWired[] = { TEXT("**Select Type**"),TEXT("Dell MS116"),TEXT("Dell MS116 275"),NULL };
TCHAR *MOUSEDELLWireLess[] = { TEXT("**Select Type**"),TEXT("Dell WM126"),TEXT("Dell WM123"),NULL };

TCHAR *MOUSELOGITECHWired[] = { TEXT("**Select Type**"),TEXT("Logitech B100"),TEXT("Logitech M90"),NULL };
TCHAR *MOUSELOGITECHWireLess[] = { TEXT("**Select Type**"),TEXT("Logitech M235"),TEXT("Logotech B170"),NULL };



TCHAR *MONITORCOM[] = {TEXT("**Select Company**"),TEXT("Samsung S19F350HNW"),TEXT(" Dell SE2416H "),TEXT("Sony Bravia"),NULL};


bool gbFullscreen = false;
WINDOWPLACEMENT wpPrev = {sizeof(WINDOWPLACEMENT)};
DWORD dwStyle;
HWND ghwnd;
HANDLE hFile;

bool IsRKeyPressed = false;
bool IsOKPressed = false;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	TCHAR lpszClassName[] = TEXT("My Win32 Project");
	HWND hwnd=NULL;
	MSG msg;

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL,IDI_APPLICATION);
	wndclass.lpszClassName = lpszClassName;
	wndclass.hCursor = LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszMenuName = NULL;

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(lpszClassName,TEXT("Welcome"),WS_OVERLAPPEDWINDOW,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,CW_USEDEFAULT,NULL,NULL,hInstance,NULL);

	if (hwnd == NULL)
	{
		MessageBox(NULL,TEXT("Window is not created"),TEXT("ERROR"),MB_OK);
		exit(0);
	}
	ghwnd = hwnd;
	ShowWindow(hwnd,SW_MAXIMIZE);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

	}

	return((int )msg.wParam);

}

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullscreen(void);
	HDC hdc =NULL;
	HDC hdc_TextOut = NULL,hdc_Text=NULL;
	PAINTSTRUCT ps,ps_TextOut;
	BITMAP bm;
	HINSTANCE hInstance;
	HDC hdcMem = NULL;
	HBITMAP hBitmapOld = NULL;
	LPPOINT pt=NULL;
	HPEN hPen = NULL;
	HFONT hMyFont = NULL,hFont=NULL;
	TCHAR str[50] = TEXT("PLEASE ENTER SPACE BAR TO CONTINUE");
	TCHAR strKey[50] = TEXT("PLEASE PRESS R  FOR RECEIPT");
	TCHAR strKeyPressed[50] = TEXT("THANK YOU FOR VISITING");
	TCHAR strKey2[50] = TEXT("Please Press Escape to Exit");
	HINSTANCE hInstanceDialog,hInstance2;
	switch (imsg)
	{
	
	case WM_CREATE:
		
		break;

	case WM_PAINT:
		if (IsOKPressed == false)
		{
			hdc = BeginPaint(ghwnd, &ps);
			//GetClientRect(ghwnd, &rc);
			hBitmap = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDBITMAP_IMAGE1));
			if (hBitmap == NULL)
			{
				MessageBox(NULL, TEXT("Could Not Load Bitmap "), TEXT("ERROR"), MB_OK);
			}
			hdcMem = CreateCompatibleDC(hdc);

			hBitmapOld = (HBITMAP)SelectObject(hdcMem, hBitmap);
			GetObject(hBitmap, sizeof(bm), &bm);
			BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);
			SelectObject(hdcMem, hBitmapOld);
			DeleteDC(hdcMem);


			hMyFont = CreateFont(30, 0, 0, 0, FW_SEMIBOLD, 15, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DECORATIVE, str);
			SelectObject(hdc, hMyFont);
			SetTextColor(hdc, RGB(255, 91, 13));
			SetBkMode(hdc, TRANSPARENT);
			TextOut(hdc, 760, 60, str, wcslen(str));
			DeleteDC(hdc);
			EndPaint(ghwnd, &ps);
			DeleteObject(hMyFont);
			
		}
		else
		{
			hdc_TextOut = BeginPaint(hwnd, &ps_TextOut);
			hPen = CreatePen(PS_SOLID, PS_SOLID, RGB(0, 255, 0));
			SelectObject(hdc_TextOut, hPen);

			//Row lines
			MoveToEx(hdc_TextOut, 20, 30, pt); //Title
			LineTo(hdc_TextOut, 1300, 30);

			MoveToEx(hdc_TextOut, 20, 50, pt); //CPU
			LineTo(hdc_TextOut, 1300, 50);

			MoveToEx(hdc_TextOut, 20, 90, pt); //RAM
			LineTo(hdc_TextOut, 1300, 90);

			MoveToEx(hdc_TextOut, 20, 130, pt); //MotherBoard
			LineTo(hdc_TextOut, 1300, 130);

			MoveToEx(hdc_TextOut, 20, 170, pt); //Graphicd Card
			LineTo(hdc_TextOut, 1300, 170);

			MoveToEx(hdc_TextOut, 20, 210, pt);//HardDisk
			LineTo(hdc_TextOut, 1300, 210);

			MoveToEx(hdc_TextOut, 20, 250, pt);//CDDVD Drive
			LineTo(hdc_TextOut, 1300, 250);

			MoveToEx(hdc_TextOut, 20, 290, pt);  //SMPS
			LineTo(hdc_TextOut, 1300, 290);

			MoveToEx(hdc_TextOut, 20, 330, pt);//CABINATE
			LineTo(hdc_TextOut, 1300, 330);

			MoveToEx(hdc_TextOut, 20, 370, pt);
			LineTo(hdc_TextOut, 1300, 370);

			MoveToEx(hdc_TextOut, 20, 410, pt);
			LineTo(hdc_TextOut, 1300, 410);

			MoveToEx(hdc_TextOut, 20, 450, pt);
			LineTo(hdc_TextOut, 1300, 450);

			MoveToEx(hdc_TextOut, 20, 490, pt);
			LineTo(hdc_TextOut, 1300, 490);

			MoveToEx(hdc_TextOut, 20, 530, pt);
			LineTo(hdc_TextOut, 1300, 530);

			//Coulmn line points
			MoveToEx(hdc_TextOut, 20, 30, pt);
			LineTo(hdc_TextOut, 20, 530);

			MoveToEx(hdc_TextOut, 200, 30, pt);
			LineTo(hdc_TextOut, 200, 490);

			MoveToEx(hdc_TextOut, 400, 30, pt);
			LineTo(hdc_TextOut, 400, 490);

			MoveToEx(hdc_TextOut, 500, 30, pt);
			LineTo(hdc_TextOut, 500, 490);

			MoveToEx(hdc_TextOut, 800, 30, pt);
			LineTo(hdc_TextOut, 800, 490);

			MoveToEx(hdc_TextOut, 1100, 30, pt);
			LineTo(hdc_TextOut, 1100, 530);

			MoveToEx(hdc_TextOut, 1300, 30, pt);
			LineTo(hdc_TextOut, 1300, 530);


			SetTextColor(hdc_TextOut, RGB(0, 0, 255));
			SetBkMode(hdc_TextOut, TRANSPARENT);

			TextOut(hdc_TextOut, 25, 60, TEXT(" CPU"), 5);
			TextOut(hdc_TextOut, 25, 100, TEXT(" RAM"), 5);
			TextOut(hdc_TextOut, 25, 140, TEXT(" MOTHERBOARD"), 13);
			TextOut(hdc_TextOut, 25, 180, TEXT(" GRAPHICD CARD"), 15);
			TextOut(hdc_TextOut, 25, 220, TEXT(" HARD DISK"), 11);
			TextOut(hdc_TextOut, 25, 260, TEXT(" CD/DVD DRIVE"), 14);
			TextOut(hdc_TextOut, 25, 300, TEXT(" SMPS"), 6);
			TextOut(hdc_TextOut, 25, 340, TEXT(" CABINATE"), 10);
			TextOut(hdc_TextOut, 25, 380, TEXT(" MOUSE"), 7);
			TextOut(hdc_TextOut, 25, 420, TEXT(" KEYBOARD"), 10);
			TextOut(hdc_TextOut, 25, 460, TEXT(" MONITOR"), 9);

			TextOut(hdc_TextOut, 995, 505, TEXT(" TOTAL COST"), 12);


			TextOut(hdc_TextOut, 1200, 35, TEXT("COST"), 5);
			TextOut(hdc_TextOut, 80, 35, TEXT("ITEMS"), 6);
			TextOut(hdc_TextOut, 250, 35, TEXT("COMPANY"), 7);
			TextOut(hdc_TextOut, 450, 35, TEXT("SIZE"), 4);
			TextOut(hdc_TextOut, 600, 35, TEXT("SUBTYPE"), 8);
			TextOut(hdc_TextOut, 900, 35, TEXT("SUBTYPE"), 8);

			hdc_Text = GetDC(hwnd);
			hFont = CreateFont(25, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DECORATIVE, strKey);
			hFont = CreateFont(25, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DECORATIVE, strKeyPressed);

			SelectObject(hdc_Text, hFont);
			SetBkMode(hdc_Text,TRANSPARENT);
			SetTextColor(hdc_Text,RGB(0,255,0));
			if(IsRKeyPressed==false)
				TextOut(hdc_Text, 530, 600, strKey, wcslen(strKey));
			else
			{
			
				TextOut(hdc_Text, 530, 600, strKeyPressed, wcslen(strKeyPressed));
				TextOut(hdc_Text, 530, 630,strKey2, wcslen(strKey2));
			}
			DeleteObject(hFont);
			DeleteDC(hdc_Text);
			hdc_Text = NULL;
			hFont = NULL;
			if (wcscmp(in.CpuType, CPU[0]) != 0 && wcscmp(in.CpuGen, CPUGENAMD[0]) != 0 && wcscmp(in.CpuArch, CPUARCHAMD[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 65, in.CpuType, wcslen(in.CpuType));
				TextOut(hdc_TextOut, 450, 65, TEXT("-"), 1);
				TextOut(hdc_TextOut, 630, 65, in.CpuGen, wcslen(in.CpuGen));
				TextOut(hdc_TextOut, 950, 65, in.CpuArch, wcslen(in.CpuArch));
			}

			//Actual Cost Text
			wsprintf(Cost.CPUText, TEXT("%d"), Cost.Cpu);
			TextOut(hdc_TextOut, 1205, 65, Cost.CPUText, wcslen(Cost.CPUText));


			//RAM Section
			if (wcscmp(in.RamCompany, RAMCOM[0]) != 0 && wcscmp(in.RamSize, RAMSIZE[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 105, in.RamCompany, wcslen(in.RamCompany));
				TextOut(hdc_TextOut, 450, 105, in.RamSize, wcslen(in.RamSize));
				TextOut(hdc_TextOut, 630, 105, TEXT("-"), 1);
				TextOut(hdc_TextOut, 950, 105, TEXT("-"), 1);
			}

			wsprintf(Cost.RAMText, TEXT("%d"), Cost.Ram);
			TextOut(hdc_TextOut, 1205, 105, Cost.RAMText, wcslen(Cost.RAMText));

			if (wcscmp(in.MotherBoardCompany, MOTHERBOARDCOM[0]) != 0 && wcscmp(in.MotherBoardType, MOTHERBOARDASUS[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 150, in.MotherBoardCompany, wcslen(in.MotherBoardCompany));
				TextOut(hdc_TextOut, 450, 150, TEXT("-"), 1);
				TextOut(hdc_TextOut, 610, 150, in.MotherBoardType, wcslen(in.MotherBoardType));
				TextOut(hdc_TextOut, 950, 150, TEXT("-"), 1);

			}

			wsprintf(Cost.MotherBoardText, TEXT("%d"), Cost.MotherBoard);
			TextOut(hdc_TextOut, 1205, 150, Cost.MotherBoardText, wcslen(Cost.MotherBoardText));


			//Graphics Card
			if (wcscmp(in.GraphicsCardCompany, GRAPHICSCARDCOM[0]) != 0 && wcscmp(in.GraphicsCardType, GRAPHICSCARDAMD[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 180, in.GraphicsCardCompany, wcslen(in.GraphicsCardCompany));
				TextOut(hdc_TextOut, 450, 180, TEXT("-"), 1);
				TextOut(hdc_TextOut, 630, 180, in.GraphicsCardType, wcslen(in.GraphicsCardType));
				TextOut(hdc_TextOut, 950, 180, TEXT("-"), 1);

			}
			wsprintf(Cost.GraphicsCardText, TEXT("%d"), Cost.GraphicsCard);
			TextOut(hdc_TextOut, 1205, 180, Cost.GraphicsCardText, wcslen(Cost.GraphicsCardText));

			//HardDisk
			if (wcscmp(in.HardDiskCompany, HARDDISK[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 220, in.HardDiskCompany, wcslen(in.HardDiskCompany));
				TextOut(hdc_TextOut, 450, 220, TEXT("1TB"), 3);
				TextOut(hdc_TextOut, 630, 220, TEXT("-"), 1);
				TextOut(hdc_TextOut, 950, 220, TEXT("-"), 1);


			}
			wsprintf(Cost.HardDiskText, TEXT("%d"), Cost.HardDisk);
			TextOut(hdc_TextOut, 1205, 220, Cost.HardDiskText, wcslen(Cost.HardDiskText));


			//CDDVD
			if (wcscmp(in.CdDvdCompany, CDDVDDRIVECOM[0]) != 0 && wcscmp(in.cdDvdType, CDDVDASUS[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 260, in.CdDvdCompany, wcslen(in.CdDvdCompany));
				TextOut(hdc_TextOut, 450, 260, TEXT("-"), 1);
				TextOut(hdc_TextOut, 620, 260, in.cdDvdType, wcslen(in.cdDvdType));
				TextOut(hdc_TextOut, 950, 260, TEXT("-"), 1);

			}
			wsprintf(Cost.CdDvdDriveText, TEXT("%d"), Cost.CdDvdDrive);
			TextOut(hdc_TextOut, 1205, 260, Cost.CdDvdDriveText, wcslen(Cost.CdDvdDriveText));


			if (wcscmp(in.SmpsCompany, SMPSCOM[0]) != 0 && wcscmp(in.SmpsType, SMPSCORSAIR[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 300, in.SmpsCompany, wcslen(in.SmpsCompany));
				TextOut(hdc_TextOut, 450, 300, TEXT("-"), 1);
				TextOut(hdc_TextOut, 620, 300, in.SmpsType, wcslen(in.SmpsType));
				TextOut(hdc_TextOut, 950, 300, TEXT("-"), 1);

			}
			wsprintf(Cost.SmpsText, TEXT("%d"), Cost.Smps);
			TextOut(hdc_TextOut, 1205, 300, Cost.SmpsText, wcslen(Cost.SmpsText));


			if (wcscmp(in.CabinateCompany, CABINATECOM[0]) != 0 && wcscmp(in.CabinateType, CABINATECORSAIR[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 340, in.CabinateCompany, wcslen(in.CabinateCompany));
				TextOut(hdc_TextOut, 450, 340, TEXT("-"), 1);
				TextOut(hdc_TextOut, 620, 340, in.CabinateType, wcslen(in.CabinateType));
				TextOut(hdc_TextOut, 950, 340, TEXT("-"), 1);

			}
			wsprintf(Cost.CabinateText, TEXT("%d"), Cost.Cabinate);
			TextOut(hdc_TextOut, 1205, 340, Cost.CabinateText, wcslen(Cost.CabinateText));


			if (wcscmp(in.MouseCompany, MOUSECOM[0]) != 0 && wcscmp(in.MouseType1, MOUSETYPE[0]) != 0 && wcscmp(in.MouseType2, MOUSEDELLWired[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 380, in.MouseCompany, wcslen(in.MouseCompany));
				TextOut(hdc_TextOut, 450, 380, TEXT("-"), 1);
				TextOut(hdc_TextOut, 630, 380, in.MouseType1, wcslen(in.MouseType1));
				TextOut(hdc_TextOut, 950, 380, in.MouseType2, wcslen(in.MouseType2));
			}

			wsprintf(Cost.MouseText, TEXT("%d"), Cost.Mouse);
			TextOut(hdc_TextOut, 1205, 380, Cost.MouseText, wcslen(Cost.MouseText));

			if (wcscmp(in.KeyboardCompany, KEYBOARDCOM[0]) != 0 && wcscmp(in.KeyBoardType1, KEYBOARDTYPE[0]) != 0 && wcscmp(in.KeyboardType2, KEYBOARDDELLWired[0]) != 0)
			{

				TextOut(hdc_TextOut, 230, 420, in.KeyboardCompany, wcslen(in.KeyboardCompany));
				TextOut(hdc_TextOut, 450, 420, TEXT("-"), 1);
				TextOut(hdc_TextOut, 630, 420, in.KeyBoardType1, wcslen(in.KeyBoardType1));
				TextOut(hdc_TextOut, 950, 420, in.KeyboardType2, wcslen(in.KeyboardType2));
			}

			wsprintf(Cost.KeyboardText, TEXT("%d"), Cost.Keyboard);
			TextOut(hdc_TextOut, 1205, 420, Cost.KeyboardText, wcslen(Cost.KeyboardText));


			if (wcscmp(in.MonitorCompany, MONITORCOM[0]) != 0)
			{

				TextOut(hdc_TextOut, 210, 460, in.MonitorCompany, wcslen(in.MonitorCompany));
				TextOut(hdc_TextOut, 450, 460, TEXT("-"), 1);
				TextOut(hdc_TextOut, 630, 460, TEXT("-"), 1);
				TextOut(hdc_TextOut, 950, 460, TEXT("-"), 1);
			}

			wsprintf(Cost.MonitorText, TEXT("%d"), Cost.Monitor);
			TextOut(hdc_TextOut, 1205, 460, Cost.MonitorText, wcslen(Cost.MonitorText));

			Cost.Total = Cost.Cpu + Cost.Ram + Cost.MotherBoard + Cost.GraphicsCard + Cost.HardDisk + Cost.Smps + Cost.Cabinate + Cost.CdDvdDrive + Cost.Keyboard + Cost.Monitor + Cost.Mouse;
			wsprintf(Cost.TotalCostText, TEXT("%d"), Cost.Total);
			TextOut(hdc_TextOut, 1205, 500, Cost.TotalCostText, wcslen(Cost.TotalCostText));



			EndPaint(hwnd, &ps_TextOut);

			DeleteDC(hdc_TextOut);
			DeleteObject(hPen);


		}
		
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{

		case VK_SPACE:
			if (IsOKPressed == false)
			{
				hInstance = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
				DialogBox(hInstance, TEXT("MyComputerShoppe"), hwnd, MyDlgProc);
			}
			break;
		case VK_ESCAPE:
			hInstance2= (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			DialogBox(hInstance2, TEXT("THANKYOU"), hwnd, MyDlgProc2);
			DestroyWindow(hwnd);
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{	
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 'R':
		case 'r':
			
			hInstanceDialog = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
			DialogBox(hInstanceDialog, TEXT("RECEIPT"), hwnd, MyDlgProc3);
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	}
	return(DefWindowProc(hwnd,imsg,wParam,lParam));
}

void ToggleFullscreen(void)
{
	MONITORINFO mi;
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd,GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = {sizeof(MONITORINFO)};
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd,GWL_STYLE,dwStyle & ~ WS_OVERLAPPEDWINDOW );
				SetWindowPos(ghwnd,HWND_TOP,mi.rcMonitor.left,mi.rcMonitor.top,mi.rcMonitor.right - mi.rcMonitor.left,mi.rcMonitor.bottom - mi.rcMonitor.top,SWP_NOZORDER|SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		SetWindowLong(ghwnd,GWL_STYLE,dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd,&wpPrev);

		SetWindowPos(ghwnd,HWND_TOP,0,0,100,100,SWP_NOMOVE|SWP_NOSIZE|SWP_NOOWNERZORDER|SWP_NOZORDER|SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

}

BOOL CALLBACK MyDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//struct MYINPUT in = { 0 };
	HBRUSH hDlgBrush=NULL;
	HINSTANCE hInstanceDialog = NULL;

	static HWND hwndCPU = NULL;
	static HWND hwndCPUGen = NULL;
	static HWND hwndCPUArch = NULL;
	static HWND hwndRamCompany = NULL;
	static HWND hwndRamSize = NULL;
	static HWND hwndMotherBoardCompany = NULL;
	static HWND hwndMotherBoardType = NULL;
	static HWND hwndGraphicsCardCompany = NULL;
	static HWND hwndGraphicsCardType = NULL;
	static HWND hwndHardDiskCompany=NULL;
	static HWND hwndCdDvdCompany = NULL;
	static HWND hwndCdDvdType = NULL;
	static HWND hwndSmpsCompany = NULL;
	static HWND hwndSmpsType = NULL;
	static HWND hwndCabinateCompany = NULL;
	static HWND hwndCabinateType = NULL;
	static HWND hwndMouseCompany = NULL;
	static HWND hwndMouseType1 = NULL;
	static HWND hwndMouseType2 = NULL;
	static HWND hwndKeyBoardCompany = NULL;
	static HWND hwndKeyBoardType1 = NULL;
	static HWND hwndKeyBoardType2 = NULL;
	static HWND hwndMonitorCompany = NULL;



	static int iSelectCPUCompany;
	static int iSelectCPUGen;
	static int iSelectCPUArch;
	static int iSelectRamCompany;
	static int iSelectRamSize;
	static int iSelectMotherBoardCompany;
	static int iSelectMotherBoardType;
	static int iSelectGraphicsCardCompany;
	static int iSelectGraphicsCardType;
	static int iSelectHardDiskCompany;
	static int iSelectCdDvdCompany;
	static int iSelectCdDvdType;
	static int iSelectSmpsCompany;
	static int iSelectSmpsType;
	static int iSelectCabinateCompany;
	static int iSelectCabinateType;
	static int iSelectKeyBoardCompany;
	static int iSelectKeyBoardType1;
	static int iSelectKeyBoardType2;
	static int iSelectMouseCompany;
	static int iSelectMouseType1;
	static int iSelectMouseType2;
	static int iSelectMonitorCompany;

	//HFONT hMyFont = NULL;
	HDC hdc = NULL;
	PAINTSTRUCT ps;
	HDC hdcMem = NULL;
	HBITMAP hBitmapOld = NULL;
	BITMAP bm;

	switch (iMsg)
	{
	case WM_INITDIALOG:

		//CPU Section
		ShowWindow(hDlg, SW_MAXIMIZE);
		SetFocus(GetDlgItem(hDlg,ID_CBCPUTYPE));
		hwndCPU = GetDlgItem(hDlg,ID_CBCPUTYPE);
		if (hwndCPU != NULL)
		{
			for (int i = 0; CPU[i] != '\0'; i++)
			{
				SendMessage(hwndCPU, CB_ADDSTRING, 0, (LPARAM)CPU[i]);
				
			}
			SendMessage(hwndCPU,CB_SETCURSEL,(WPARAM)0,(LPARAM)0);
		}

		hwndCPUGen = GetDlgItem(hDlg, ID_CBCPUGEN);
		ShowWindow(hwndCPUGen, SW_HIDE);
		
		
		hwndCPUArch = GetDlgItem(hDlg,ID_CBCPUARCH);
		ShowWindow(hwndCPUArch,SW_HIDE);

		//RAM Section
		SetFocus(GetDlgItem(hDlg, ID_CBRAMCOM));
		hwndRamCompany = GetDlgItem(hDlg, ID_CBRAMCOM);

		if (hwndRamCompany != NULL)
		{
			for (int i = 0; RAMCOM[i] != '\0'; i++)
			{
				SendMessage(hwndRamCompany, CB_ADDSTRING, 0, (LPARAM)RAMCOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndRamCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		//SetFocus(GetDlgItem(hDlg, ID_CBRAMSIZE));
		hwndRamSize = GetDlgItem(hDlg, ID_CBRAMSIZE);
		ShowWindow(hwndRamSize, SW_HIDE);




		//MotherBoard Section
		SetFocus(GetDlgItem(hDlg, ID_CBMOTHERBOARDCOM));
		hwndMotherBoardCompany = GetDlgItem(hDlg, ID_CBMOTHERBOARDCOM);

		if (hwndMotherBoardCompany != NULL)
		{
			for (int i = 0; MOTHERBOARDCOM[i] != '\0'; i++)
			{
				SendMessage(hwndMotherBoardCompany, CB_ADDSTRING, 0, (LPARAM)MOTHERBOARDCOM[i]);
			}
			SendMessage(hwndMotherBoardCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}
		
		//SetFocus(GetDlgItem(hDlg, ID_CBMOTHERBOARDTYPE));
		hwndMotherBoardType = GetDlgItem(hDlg, ID_CBMOTHERBOARDTYPE);
		ShowWindow(hwndMotherBoardType,SW_HIDE);


		//Graphics card Section

		SetFocus(GetDlgItem(hDlg, ID_CBGRAPHICSCARDCOM));
		hwndGraphicsCardCompany = GetDlgItem(hDlg, ID_CBGRAPHICSCARDCOM);

		if (hwndGraphicsCardCompany != NULL)
		{
			for (int i = 0; GRAPHICSCARDCOM[i] != '\0'; i++)
			{
				SendMessage(hwndGraphicsCardCompany, CB_ADDSTRING, 0, (LPARAM)GRAPHICSCARDCOM[i]);

			}
			SendMessage(hwndGraphicsCardCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		//SetFocus(GetDlgItem(hDlg, ID_CBGRAPHICSCARDTYPE));
		hwndGraphicsCardType= GetDlgItem(hDlg, ID_CBGRAPHICSCARDTYPE);
		ShowWindow(hwndGraphicsCardType, SW_HIDE);


		//HardDisk
		SetFocus(GetDlgItem(hDlg, ID_CBHARDDISKCOM));
		hwndHardDiskCompany = GetDlgItem(hDlg, ID_CBHARDDISKCOM);

		if (hwndHardDiskCompany != NULL)
		{
			for (int i = 0; HARDDISK[i] != NULL; i++)
			{
				SendMessage(hwndHardDiskCompany, CB_ADDSTRING, 0, (LPARAM)HARDDISK[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndHardDiskCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		//CDDVD Section

		SetFocus(GetDlgItem(hDlg, ID_CBCDDVDCOM));
		hwndCdDvdCompany = GetDlgItem(hDlg, ID_CBCDDVDCOM);

		if (hwndCPU != NULL)
		{
			for (int i = 0; CDDVDDRIVECOM[i] != '\0'; i++)
			{
				SendMessage(hwndCdDvdCompany, CB_ADDSTRING, 0, (LPARAM)CDDVDDRIVECOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndCdDvdCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		//SetFocus(GetDlgItem(hDlg, ID_CBCDDVDTYPE));
		hwndCdDvdType = GetDlgItem(hDlg, ID_CBCDDVDTYPE);
		ShowWindow(hwndCdDvdType,SW_HIDE);

		//SMPS 

		SetFocus(GetDlgItem(hDlg, ID_CBSMPSCOM));
		hwndSmpsCompany = GetDlgItem(hDlg, ID_CBSMPSCOM);

		if (hwndSmpsCompany != NULL)
		{
			for (int i = 0; SMPSCOM[i] != '\0'; i++)
			{
				SendMessage(hwndSmpsCompany, CB_ADDSTRING, 0, (LPARAM)SMPSCOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndSmpsCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		hwndSmpsType = GetDlgItem(hDlg, ID_CBSMPSTYPE);
		ShowWindow(hwndSmpsType,SW_HIDE);


		SetFocus(GetDlgItem(hDlg, ID_CBCABINATECOM));
		hwndCabinateCompany = GetDlgItem(hDlg, ID_CBCABINATECOM);

		if (hwndCabinateCompany != NULL)
		{
			for (int i = 0; CABINATECOM[i] != '\0'; i++)
			{
				SendMessage(hwndCabinateCompany, CB_ADDSTRING, 0, (LPARAM)CABINATECOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndCabinateCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		hwndCabinateType = GetDlgItem(hDlg, ID_CBCABINATETYPE);
		ShowWindow(hwndCabinateType, SW_HIDE);


		//MouseSection
		SetFocus(GetDlgItem(hDlg, ID_CBMOUSECOM));
		hwndMouseCompany = GetDlgItem(hDlg, ID_CBMOUSECOM);

		if (hwndMouseCompany != NULL)
		{
			for (int i = 0; MOUSECOM[i] != '\0'; i++)
			{
				SendMessage(hwndMouseCompany, CB_ADDSTRING, WPARAM(0), (LPARAM)MOUSECOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndMouseCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		
		hwndMouseType1 = GetDlgItem(hDlg, ID_CBMOUSETYPE);
		ShowWindow(hwndMouseType1,SW_HIDE);
		
		hwndMouseType2 = GetDlgItem(hDlg, ID_CBMOUSESUBTYPE);
		ShowWindow(hwndMouseType2, SW_HIDE);



		//KeyBoard Section
		SetFocus(GetDlgItem(hDlg, ID_CBKEYBOARDCOM));
		hwndKeyBoardCompany = GetDlgItem(hDlg, ID_CBKEYBOARDCOM);

		if (hwndKeyBoardCompany != NULL)
		{
			for (int i = 0; KEYBOARDCOM[i] != '\0'; i++)
			{
				SendMessage(hwndKeyBoardCompany, CB_ADDSTRING, WPARAM(0), (LPARAM)KEYBOARDCOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndKeyBoardCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}

		hwndKeyBoardType1 = GetDlgItem(hDlg, ID_CBKEYBOARDTYPE);
		ShowWindow(hwndKeyBoardType1, SW_HIDE);


		hwndKeyBoardType2 = GetDlgItem(hDlg, ID_CBKEYBOARDSUBTYPE);
		ShowWindow(hwndKeyBoardType2, SW_HIDE);


		SetFocus(GetDlgItem(hDlg, ID_CBMONITORCOM));
		hwndMonitorCompany = GetDlgItem(hDlg, ID_CBMONITORCOM);

		if (hwndMonitorCompany != NULL)
		{
			for (int i = 0; MONITORCOM[i] != '\0'; i++)
			{
				SendMessage(hwndMonitorCompany, CB_ADDSTRING, WPARAM(0), (LPARAM)MONITORCOM[i]);

				//SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)A);
			}
			SendMessage(hwndMonitorCompany, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
		}



		



		return (TRUE);


	case WM_PAINT:
		
		hdc = BeginPaint(hDlg,&ps);
		//hMyFont = CreateFont(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DECORATIVE, str);


		hBitmap = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDBITMAP_IMAGE2 ));
		if (hBitmap == NULL)
		{
			MessageBox(NULL, TEXT("Could Not Load Bitmap "), TEXT("ERROR"), MB_OK);
		}
		hdcMem = CreateCompatibleDC(hdc);

		hBitmapOld = (HBITMAP)SelectObject(hdcMem, hBitmap);
		GetObject(hBitmap, sizeof(bm), &bm);
		BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);
		SelectObject(hdcMem, hBitmapOld);
		DeleteDC(hdcMem);


		SetTextColor(hdc,RGB(0,255,255));
		SetBkMode(hdc, TRANSPARENT);
//		SelectObject(hdc,hMyFont);
		TextOut(hdc, 70, 25, TEXT(" CPU"), 5);
		TextOut(hdc, 70, 85, TEXT(" RAM"), 5);
		TextOut(hdc, 70, 145, TEXT(" MOTHERBOARD"), 13);
		TextOut(hdc, 70, 205, TEXT(" GRAPHICD CARD"), 15);
		TextOut(hdc, 72, 265, TEXT(" HARD DISK"), 11);
		TextOut(hdc, 70, 325, TEXT(" CD/DVD DRIVE"), 14);
		TextOut(hdc, 70, 385, TEXT(" SMPS"), 6);
		TextOut(hdc, 70, 445, TEXT(" CABINATE"), 10);
		TextOut(hdc, 70, 505, TEXT(" MOUSE"), 7);
		TextOut(hdc, 70, 565, TEXT(" KEYBOARD"), 10);
		TextOut(hdc, 70, 625,TEXT(" MONITOR"), 9);
	
		
		EndPaint(hDlg,&ps);


		return(TRUE);
	case WM_CTLCOLORDLG:
		hDlgBrush=CreateSolidBrush(RGB(0,0,0));
		return((BOOL)hDlgBrush);
	
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			
		GetDlgItemText(hDlg,ID_CBCPUTYPE,in.CpuType,50);
		GetDlgItemText(hDlg, ID_CBCPUGEN, in.CpuGen, 50);
		GetDlgItemText(hDlg,ID_CBCPUARCH,in.CpuArch,50);

		GetDlgItemText(hDlg,ID_CBRAMCOM,in.RamCompany,50);
		GetDlgItemText(hDlg,ID_CBRAMSIZE,in.RamSize,50);
		
		GetDlgItemText(hDlg,ID_CBMOTHERBOARDCOM,in.MotherBoardCompany,50);
		GetDlgItemText(hDlg,ID_CBMOTHERBOARDTYPE,in.MotherBoardType,50);
		
		GetDlgItemText(hDlg,ID_CBGRAPHICSCARDCOM,in.GraphicsCardCompany,50);
		GetDlgItemText(hDlg,ID_CBGRAPHICSCARDTYPE,in.GraphicsCardType,50);
		
		GetDlgItemText(hDlg,ID_CBHARDDISKCOM,in.HardDiskCompany,50);
		
		GetDlgItemText(hDlg, ID_CBCDDVDCOM, in.CdDvdCompany, 50);
		GetDlgItemText(hDlg, ID_CBCDDVDTYPE,in.cdDvdType,50 );
		
		GetDlgItemText(hDlg,ID_CBSMPSCOM,in.SmpsCompany,50);
		GetDlgItemText(hDlg,ID_CBSMPSTYPE,in.SmpsType,50);
		
		GetDlgItemText(hDlg,ID_CBCABINATECOM,in.CabinateCompany,50);
		GetDlgItemText(hDlg,ID_CBCABINATETYPE,in.CabinateType,50);

		GetDlgItemText(hDlg, ID_CBMOUSECOM, in.MouseCompany, 50);
		GetDlgItemText(hDlg, ID_CBMOUSETYPE, in.MouseType1, 50);
		GetDlgItemText(hDlg, ID_CBMOUSESUBTYPE, in.MouseType2, 50);


		GetDlgItemText(hDlg,ID_CBKEYBOARDCOM,in.KeyboardCompany,50);
		GetDlgItemText(hDlg,ID_CBKEYBOARDTYPE,in.KeyBoardType1,50);
		GetDlgItemText(hDlg,ID_CBKEYBOARDSUBTYPE,in.KeyboardType2,50);

		GetDlgItemText(hDlg,ID_CBMONITORCOM,in.MonitorCompany,50);

		IsOKPressed = true;
		
	
		InvalidateRect(ghwnd,NULL,TRUE);
			EndDialog(hDlg,0);
			break;
		case IDCANCEL:
			
			EndDialog(hDlg,0);
			break;

		}

		switch (HIWORD(wParam))
		{
		case CBN_SELCHANGE:
			
			SetFocus(GetDlgItem(hwndCPU, ID_CBCPUTYPE));
			iSelectCPUCompany = SendMessage(hwndCPU, CB_GETCURSEL, WPARAM(0), LPARAM(0));
			if ((iSelectCPUCompany != CB_ERR) && (iSelectCPUCompany != 0))
			{
				ShowWindow(hwndCPUGen, SW_SHOW);
			}

				if( (iSelectCPUCompany==1) &&( HWND(lParam)==hwndCPU))
				{
					SetFocus(GetDlgItem(hwndCPUGen, ID_CBCPUGEN));
					SendMessage(hwndCPUGen, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CPUGENAMD[i] != NULL; i++)
					{
						SendMessage(hwndCPUGen, CB_ADDSTRING, WPARAM(0), LPARAM(CPUGENAMD[i]));
					}
					SendMessage(hwndCPUGen, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					SendMessage(hwndCPUArch, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					//SendMessage(hwndCPUArch, CB_, WPARAM(0), LPARAM(0));
				}
			

				if ((iSelectCPUCompany == 2) && (HWND(lParam) == hwndCPU))
				{
					SetFocus(GetDlgItem(hwndCPUGen, ID_CBCPUGEN));
					SendMessage(hwndCPUGen, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CPUGENINTEL[i] != NULL; i++)
					{
						SendMessage(hwndCPUGen, CB_ADDSTRING, WPARAM(0), LPARAM(CPUGENINTEL[i]));
					}
					SendMessage(hwndCPUGen, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					SendMessage(hwndCPUArch, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					//SendMessage(hwndCPUGen, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg,ID_CBCPUGEN));
				iSelectCPUGen = SendMessage(hwndCPUGen, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				if ((iSelectCPUGen != CB_ERR) && (iSelectCPUGen != 0))
				{
					ShowWindow(hwndCPUArch,SW_SHOW);
				}
				if ((iSelectCPUCompany == 1)&& (iSelectCPUGen!=0) && (HWND(lParam) == hwndCPUGen))
				{
					SetFocus(GetDlgItem(hwndCPUArch,ID_CBCPUARCH));
					SendMessage(hwndCPUArch,CB_RESETCONTENT,WPARAM(0),LPARAM(0));
					for (int i = 0; CPUARCHAMD[i] != NULL; i++)
					{
						SendMessage(hwndCPUArch,CB_ADDSTRING,WPARAM(0),LPARAM(CPUARCHAMD[i]));
					}
					SendMessage(hwndCPUArch,CB_SETCURSEL,WPARAM(0),LPARAM(0));
					//MessageBox(NULL, TEXT("in AMD"), TEXT("ERROR"), MB_OK);
				}

				SetFocus(GetDlgItem(hDlg, ID_CBCPUGEN));
				iSelectCPUGen = SendMessage(hwndCPUGen, CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				
				if ((iSelectCPUGen != CB_ERR) && (iSelectCPUGen != 0))
				{
					ShowWindow(hwndCPUArch, SW_SHOW);
				}
				if ((iSelectCPUCompany == 2) && (iSelectCPUGen != 0) && (HWND(lParam) == hwndCPUGen))
				{
					SetFocus(GetDlgItem(hwndCPUArch, ID_CBCPUARCH));
					SendMessage(hwndCPUArch, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CPUARCHINTEL[i] != NULL; i++)
					{
						SendMessage(hwndCPUArch, CB_ADDSTRING, WPARAM(0), LPARAM(CPUARCHINTEL[i]));
					}
					SendMessage(hwndCPUArch, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					//MessageBox(NULL, TEXT("In Intel"), TEXT("ERROR"), MB_OK);
				}
				

				
				SetFocus(GetDlgItem(hDlg,ID_CBCPUARCH));
				iSelectCPUArch = SendMessage(hwndCPUArch, CB_GETCURSEL, WPARAM(0), LPARAM(0));
					if (iSelectCPUCompany != 0 && iSelectCPUGen != 0 && iSelectCPUArch != 0)
				{
					switch (iSelectCPUCompany)
					{
					case 1:
						switch (iSelectCPUGen)
						{

						case 1:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 10000;
								break;
							case 2:
								Cost.Cpu = 15000;
								break;
							case 3:
								Cost.Cpu = 20000;
								break;
							default:
								break;
							}
							break;
						case 2:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 12000;
								break;
							case 2:
								Cost.Cpu = 17000;
								break;
							case 3:
								Cost.Cpu = 22000;
								break;
							default:
								break;
							}
							break;
						case 3:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 45000;
								break;
							case 2:
								Cost.Cpu = 49000;
								break;
							case 3:
								Cost.Cpu = 52000;
								break;
							default:
								break;
							}
							break;
						default:
							break;
						}
							break;
					case 2:
						switch (iSelectCPUGen)
						{

						case 1:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 12000;
								break;
							case 2:
								Cost.Cpu = 17000;
								break;
							case 3:
								Cost.Cpu = 22000;
								break;
							default:
								break;
							}
							break;
						case 2:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 18000;
								break;
							case 2:
								Cost.Cpu = 20000;
								break;
							case 3:
								Cost.Cpu = 27000;
								break;
							default:
								break;
							}
							break;
						case 3:
							switch (iSelectCPUArch)
							{
							case 1:
								Cost.Cpu = 40000;
								break;
							case 2:
								Cost.Cpu = 50000;
								break;
							case 3:
								Cost.Cpu = 55000;
								break;
							default:
								break;
							}
							break;
						default:
							break;
						}
						break;

						
					}
				}
					else
					{
						Cost.Cpu = 0;
						
					}


				//RAM Section for Calculation
				SetFocus(GetDlgItem(hDlg, ID_CBRAMCOM));
				iSelectRamCompany = SendMessage(hwndRamCompany, CB_GETCURSEL, WPARAM(0), LPARAM(0));

				if ((iSelectRamCompany != CB_ERR) && (iSelectRamCompany != 0))
				{
					ShowWindow(hwndRamSize, SW_SHOW);
				}

				if ((iSelectRamCompany != 0) && ((HWND)lParam == hwndRamCompany))
				{
					SetFocus(GetDlgItem(hwndRamSize, ID_CBRAMSIZE));
					SendMessage(hwndRamSize, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; RAMSIZE[i] != NULL; i++)
					{
						SendMessage(hwndRamSize, CB_ADDSTRING, 0, (LPARAM)RAMSIZE[i]);


					}
					SendMessage(hwndRamSize, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);
				}

				SetFocus(GetDlgItem(hDlg, ID_CBRAMSIZE));
				iSelectRamSize = SendMessage(hwndRamSize,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectRamCompany != 0) && (iSelectRamSize != 0))
				{
					switch (iSelectRamCompany)
					{
					case 1:
						switch (iSelectRamSize)
						{
						case 1:
							Cost.Ram = 4363;
							break;
						case 2:
							Cost.Ram = 5309;
							break;
						case 3:
							Cost.Ram = 14599;
						}
						break;
					case 2:
						switch (iSelectRamSize)
						{
						case 1:
							Cost.Ram = 2599;
							break;
						case 2:
							Cost.Ram = 5590;
							break;
						case 3:
							Cost.Ram = 10500;
							break;
						}
						break;
					case 3:
						switch (iSelectRamSize)
						{
						case 1:
							Cost.Ram = 3999;
							break;
						case 2:
							Cost.Ram = 10000;
							break;
						case 3:
							Cost.Ram = 15999;
						}
						break;

					}
				}
				else {
					Cost.Ram = 0;
				}

			//MotherBoard Section
				SetFocus(GetDlgItem(hDlg, ID_CBMOTHERBOARDCOM));
				iSelectMotherBoardCompany = SendMessage(hwndMotherBoardCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));

				if ((iSelectMotherBoardCompany != CB_ERR) && (iSelectMotherBoardCompany != 0))
				{
					ShowWindow(hwndMotherBoardType,SW_SHOW);
				}

				if ((iSelectMotherBoardCompany == 1) && ((HWND)lParam == hwndMotherBoardCompany))
				{
					SetFocus(hwndMotherBoardType);
					SendMessage(hwndMotherBoardType,CB_RESETCONTENT,WPARAM(0),LPARAM(0));
					for (int i = 0; MOTHERBOARDASUS[i] != NULL; i++)
					{
						SendMessage(hwndMotherBoardType,CB_ADDSTRING,WPARAM(0),LPARAM(MOTHERBOARDASUS[i]));

					}
					SendMessage(hwndMotherBoardType,CB_SETCURSEL,WPARAM(0),LPARAM(0));
				}

				if ((iSelectMotherBoardCompany == 2) && ((HWND)lParam == hwndMotherBoardCompany))
				{
					SetFocus(hwndMotherBoardType);
					SendMessage(hwndMotherBoardType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOTHERBOARDBIOSTAR[i] != NULL; i++)
					{
						SendMessage(hwndMotherBoardType, CB_ADDSTRING, WPARAM(0), LPARAM(MOTHERBOARDBIOSTAR[i]));

					}
					SendMessage(hwndMotherBoardType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectMotherBoardCompany == 3) && ((HWND)lParam == hwndMotherBoardCompany))
				{
					SetFocus(hwndMotherBoardType);
					SendMessage(hwndMotherBoardType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOTHERBOARDGIGABYTE[i] != NULL; i++)
					{
						SendMessage(hwndMotherBoardType, CB_ADDSTRING, WPARAM(0), LPARAM(MOTHERBOARDGIGABYTE[i]));

					}
					SendMessage(hwndMotherBoardType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg, ID_CBMOTHERBOARDTYPE));
				iSelectMotherBoardType = SendMessage(hwndMotherBoardType,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				
				if ((iSelectMotherBoardCompany != 0) && (iSelectMotherBoardType != 0))
				{
					switch (iSelectMotherBoardCompany)
					{
						case 1:
							switch (iSelectMotherBoardType)
							{
							case 1:
								Cost.MotherBoard = 8000;
								break;
							case 2:
								Cost.MotherBoard = 9000;
								break;
							}
							break;

						case 2:
							switch (iSelectMotherBoardType)
							{
							case 1:
								Cost.MotherBoard = 5400;
								break;
							case 2:
								Cost.MotherBoard = 11580;
								break;
							}
							break;
						case 3:
							switch (iSelectMotherBoardType)
							{
							case 1:
								Cost.MotherBoard = 4429;
								break;
							case 2:
								Cost.MotherBoard = 4590;
								break;
							}
							break;
						}
				}
				else
				{
					Cost.MotherBoard = 0;
					
				}
				

				//Graphics card Section
				SetFocus(GetDlgItem(hDlg,ID_CBGRAPHICSCARDCOM));
				
				hwndGraphicsCardCompany = GetDlgItem(hDlg,ID_CBGRAPHICSCARDCOM);
				iSelectGraphicsCardCompany = SendMessage(hwndGraphicsCardCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectGraphicsCardCompany != CB_ERR) && (iSelectGraphicsCardCompany != 0))
				{
					ShowWindow(hwndGraphicsCardType,SW_SHOW);
				}
				if ((iSelectGraphicsCardCompany == 1) && (HWND)lParam == hwndGraphicsCardCompany)
				{
					SetFocus(hwndGraphicsCardType);
					SendMessage(hwndGraphicsCardType,CB_RESETCONTENT,WPARAM(0),LPARAM(0));
					for (int i = 0; GRAPHICSCARDAMD[i] != NULL; i++)
					{
						SendMessage(hwndGraphicsCardType,CB_ADDSTRING,WPARAM(0),LPARAM(GRAPHICSCARDAMD[i]));
					}
					SendMessage(hwndGraphicsCardType,CB_SETCURSEL,WPARAM(0),LPARAM(0));
				}

				if ((iSelectGraphicsCardCompany == 2) && (HWND)lParam == hwndGraphicsCardCompany)
				{
					SetFocus(hwndGraphicsCardType);
					SendMessage(hwndGraphicsCardType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; GRAPHICSCARDNVDIA[i] != NULL; i++)
					{
						SendMessage(hwndGraphicsCardType, CB_ADDSTRING, WPARAM(0), LPARAM(GRAPHICSCARDNVDIA[i]));
					}
					SendMessage(hwndGraphicsCardType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				GetDlgItem(hDlg,ID_CBGRAPHICSCARDTYPE);
				SetFocus(GetDlgItem(hDlg, ID_CBGRAPHICSCARDTYPE));
				iSelectGraphicsCardType = SendMessage(hwndGraphicsCardType,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectGraphicsCardCompany != 0) && (iSelectGraphicsCardType != 0))
				{
					switch (iSelectGraphicsCardCompany)
					{
					case 1:
						switch (iSelectGraphicsCardType)
						{
						case 1:
							Cost.GraphicsCard = 14482;
							break;
						case 2:
							Cost.GraphicsCard = 15012;
							break;

						}
						break;

					case 2:
						switch (iSelectGraphicsCardType)
						{
						case 1:
							Cost.GraphicsCard = 14600;
							break;
						case 2:
							Cost.GraphicsCard = 9300;
							break;
						default:
							break;
						}

						break;
					}
				}
				else
				{
					Cost.GraphicsCard = 0;
				}

				//HardDisk
				//SetFocus(GetDlgItem(hDlg,ID_CBHARDDISKCOM));
				GetDlgItem(hDlg,ID_CBHARDDISKCOM);
				SetFocus(GetDlgItem(hDlg, ID_CBHARDDISKCOM));
				iSelectHardDiskCompany = SendMessage(hwndHardDiskCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if (iSelectHardDiskCompany != 0)
				{
					switch (iSelectHardDiskCompany)
					{
					case 1:
						Cost.HardDisk = 4229;
						break;
					case 2:
						Cost.HardDisk = 4750;
						break;
					case 3:
						Cost.HardDisk = 4444;
						break;

					default:
						Cost.HardDisk = 0;
						break;
					}
				}


				//CD DVD Drive
				SetFocus(GetDlgItem(hDlg, ID_CBCDDVDCOM));
				iSelectCdDvdCompany = SendMessage(hwndCdDvdCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectCdDvdCompany != CB_ERR) && (iSelectCdDvdCompany != 0))
				{
					ShowWindow(hwndCdDvdType,SW_SHOW);
				}
				if ((iSelectCdDvdCompany == 1) && ((HWND)lParam == hwndCdDvdCompany))
				{
					SetFocus(hwndCdDvdType);
					SendMessage(hwndCdDvdType,CB_RESETCONTENT,WPARAM(0),LPARAM(0));
					for (int i = 0; CDDVDDELL[i] != NULL; i++)
					{
						SendMessage(hwndCdDvdType,CB_ADDSTRING,WPARAM(0),LPARAM(CDDVDDELL[i]));

					}
					SendMessage(hwndCdDvdType,CB_SETCURSEL,WPARAM(0),LPARAM(0));
				}

				
				if ((iSelectCdDvdCompany == 2) && ((HWND)lParam == hwndCdDvdCompany))
				{
					SetFocus(hwndCdDvdType);
					SendMessage(hwndCdDvdType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CDDVDHP[i] != NULL; i++)
					{
						SendMessage(hwndCdDvdType, CB_ADDSTRING, WPARAM(0), LPARAM(CDDVDHP[i]));

					}
					SendMessage(hwndCdDvdType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectCdDvdCompany == 3) && ((HWND)lParam == hwndCdDvdCompany))
				{
					SetFocus(hwndCdDvdType);
					SendMessage(hwndCdDvdType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CDDVDASUS[i] != NULL; i++)
					{
						SendMessage(hwndCdDvdType, CB_ADDSTRING, WPARAM(0), LPARAM(CDDVDASUS[i]));

					}
					SendMessage(hwndCdDvdType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg, ID_CBCDDVDTYPE));
				iSelectCdDvdType = SendMessage(hwndCdDvdType,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectCdDvdCompany != CB_ERR) && (iSelectCdDvdType != CB_ERR))
				{
					switch (iSelectCdDvdCompany)
					{
					case 1:
						switch (iSelectCdDvdType)
						{
						case 0:
							Cost.CdDvdDrive = 0;
							break;
						case 1:
							Cost.CdDvdDrive = 1028;
							break;
						case 2:
							Cost.CdDvdDrive = 1700;
							break;
						default:
							Cost.CdDvdDrive = 0;
							break;
						}
						break;
					case 2:
						switch (iSelectCdDvdType)
						{
						case 0:
							Cost.CdDvdDrive = 0;
							break;
						case 1:
							Cost.CdDvdDrive = 1022;
							break;
						case 2:
							Cost.CdDvdDrive = 1499;
							break;
						default:
							Cost.CdDvdDrive = 0;
							break;
						}
						break;
					case 3:
						switch (iSelectCdDvdType)
						{
						case 0:
							Cost.CdDvdDrive = 0;
							break;
						case 1:
							Cost.CdDvdDrive = 1500;
							break;
						case 2:
							Cost.CdDvdDrive = 2000;
							break;
						default:
							Cost.CdDvdDrive = 0;
							break;
						}
						break;
					default:
						break;
					}
				}


				//SMPS Section

				SetFocus(GetDlgItem(hDlg, ID_CBSMPSCOM));
				iSelectSmpsCompany = SendMessage(hwndSmpsCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectSmpsCompany != CB_ERR) && (iSelectSmpsCompany != 0))
				{
					ShowWindow(hwndSmpsType,SW_SHOW);
				}
				if ((iSelectSmpsCompany == 1) && ((HWND)lParam == hwndSmpsCompany))
				{
					SetFocus(hwndSmpsType);
					SendMessage(hwndSmpsType,CB_RESETCONTENT,WPARAM(0),LPARAM(0));
					for (int i = 0; SMPSCORSAIR[i] != NULL; i++)
					{
						SendMessage(hwndSmpsType,CB_ADDSTRING,WPARAM(0),LPARAM(SMPSCORSAIR[i]));
					}
					SendMessage(hwndSmpsType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectSmpsCompany == 2) && ((HWND)lParam == hwndSmpsCompany))
				{
					SetFocus(hwndSmpsType);
					SendMessage(hwndSmpsType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; SMPSDELL[i] != NULL; i++)
					{
						SendMessage(hwndSmpsType, CB_ADDSTRING, WPARAM(0), LPARAM(SMPSDELL[i]));
					}
					SendMessage(hwndSmpsType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg, ID_CBSMPSTYPE));
				iSelectSmpsType = SendMessage(hwndSmpsType,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectSmpsCompany != CB_ERR) && (iSelectSmpsType != CB_ERR))
				{
					switch (iSelectSmpsCompany)
					{
					case 1:
						switch (iSelectSmpsType)
						{
						case 0:
							Cost.Smps = 0;
							break;
						case 1:
							Cost.Smps = 6000;
							break;
						case 2:
							Cost.Smps = 3000;
							break;


						}
						break;

					case 2:
						switch (iSelectSmpsType)
						{
						case 0:
							Cost.Smps = 0;
							break;
						case 1:
							Cost.Smps = 8628;
							break;
						case 2:
							Cost.Smps = 8448;
							break;


						}
						break;
					default :
						break;

					}
				}


				//CABIANATE Section
				SetFocus(GetDlgItem(hDlg, ID_CBCABINATECOM));
				iSelectCabinateCompany = SendMessage(hwndCabinateCompany, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectCabinateCompany != CB_ERR) && (iSelectCabinateCompany != 0))
				{
					ShowWindow(hwndCabinateType, SW_SHOW);
				}
				if ((iSelectCabinateCompany == 1) && ((HWND)lParam == hwndCabinateCompany))
				{
					SetFocus(hwndCabinateType);
					SendMessage(hwndCabinateType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CABINATEDELL[i] != NULL; i++)
					{
						SendMessage(hwndCabinateType, CB_ADDSTRING, WPARAM(0), LPARAM(CABINATEDELL[i]));
					}
					SendMessage(hwndCabinateType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectCabinateCompany == 2) && ((HWND)lParam == hwndCabinateCompany))
				{
					SetFocus(hwndCabinateType);
					SendMessage(hwndCabinateType, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; CABINATEDELL[i] != NULL; i++)
					{
						SendMessage(hwndCabinateType, CB_ADDSTRING, WPARAM(0), LPARAM(CABINATECORSAIR[i]));
					}
					SendMessage(hwndCabinateType, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg, ID_CBCABINATETYPE));
				iSelectCabinateType = SendMessage(hwndCabinateType, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectCabinateCompany != CB_ERR) && (iSelectCabinateType != CB_ERR))
				{
					switch (iSelectCabinateCompany)
					{
					case 1:
						switch (iSelectCabinateType)
						{
						case 0:
							Cost.Cabinate = 0;
							break;
						case 1:
							Cost.Cabinate = 6399;
							break;
						case 2:
							Cost.Cabinate = 5227;
							break;


						}
						break;

					case 2:
						switch (iSelectCabinateType)
						{
						case 0:
							Cost.Cabinate = 0;
							break;
						case 1:
							Cost.Cabinate = 3537;
							break;
						case 2:
							Cost.Cabinate = 5711;
							break;
						}
						break;
					default:
						Cost.Cabinate = 0;
						break;

					}
				}

				//Mouse Section
				SetFocus(GetDlgItem(hDlg, ID_CBMOUSECOM));
				iSelectMouseCompany = SendMessage(hwndMouseCompany, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectMouseCompany != CB_ERR) && (iSelectMouseCompany != 0))
				{
					ShowWindow(hwndMouseType1, SW_SHOW);
				}

				if ((iSelectMouseCompany!=0 ) && ((HWND)lParam == hwndMouseCompany))
				{
					SetFocus(hwndMouseType1);
					SendMessage(hwndMouseType1, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSETYPE[i] != NULL; i++)
					{
						SendMessage(hwndMouseType1, CB_ADDSTRING, WPARAM(0), LPARAM(MOUSETYPE[i]));
					}
					SendMessage(hwndMouseType1, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					SendMessage(hwndMouseType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				iSelectMouseType1 = SendMessage(hwndMouseType1,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectMouseType1 != CB_ERR) && iSelectMouseType1 != 0)
				{
					ShowWindow(hwndMouseType2,SW_SHOW);
				}
				if ((iSelectMouseType1 == 1) && (iSelectMouseCompany == 1) && ((HWND)lParam == hwndMouseType1))
				{
					SetFocus(hwndMouseType2);
					SendMessage(hwndMouseType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSEDELLWired[i] != NULL; i++)
					{
						SendMessage(hwndMouseType2, CB_ADDSTRING, WPARAM(0), LPARAM(MOUSEDELLWired[i]));
					}
					SendMessage(hwndMouseType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}
					
				if ((iSelectMouseType1 == 2) && (iSelectMouseCompany == 1) && ((HWND)lParam == hwndMouseType1))
				{
					SetFocus(hwndMouseType2);
					SendMessage(hwndMouseType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSEDELLWireLess[i] != NULL; i++)
					{
						SendMessage(hwndMouseType2, CB_ADDSTRING, WPARAM(0), LPARAM(MOUSEDELLWireLess[i]));
					}
					SendMessage(hwndMouseType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectMouseType1 == 1) && (iSelectMouseCompany == 2) && ((HWND)lParam == hwndMouseType1))
				{
					SetFocus(hwndMouseType2);
					SendMessage(hwndMouseType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSELOGITECHWired[i] != NULL; i++)
					{
						SendMessage(hwndMouseType2, CB_ADDSTRING, WPARAM(0), LPARAM(MOUSELOGITECHWired[i]));
					}
					SendMessage(hwndMouseType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectMouseType1 == 2) && (iSelectMouseCompany == 2) && ((HWND)lParam == hwndMouseType1))
				{
					SetFocus(hwndMouseType2);
					SendMessage(hwndMouseType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSELOGITECHWireLess[i] != NULL; i++)
					{
						SendMessage(hwndMouseType2, CB_ADDSTRING, WPARAM(0), LPARAM(MOUSELOGITECHWireLess[i]));
					}
					SendMessage(hwndMouseType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}
					
				SetFocus(GetDlgItem(hDlg,ID_CBMOUSESUBTYPE));
				iSelectMouseType2 = SendMessage(hwndMouseType2,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if ((iSelectMouseCompany != 0) && (iSelectMouseType1 != 0) && (iSelectMouseType2 != 0))
				{
					switch (iSelectMouseCompany)
					{
					case 1:
						switch (iSelectMouseType1)
						{
						case 1:
							switch (iSelectMouseType2)
							{
							case 1:
								Cost.Mouse = 400;
								break;
							case 2:
								Cost.Mouse = 800;
							}
							break;

						case 2:
							switch (iSelectMouseType2)
							{
							case 1:
								Cost.Mouse = 1500;
								break;
							case 2:
								Cost.Mouse = 1800;
								break;
							}
							break;
						}
						break;

					case 2:
						switch (iSelectMouseType1)
						{
						case 1:
							switch (iSelectMouseType2)
							{
							case 1:
								Cost.Mouse = 300;
								break;
							case 2:
								Cost.Mouse = 600;
							}
							break;

						case 2:
							switch (iSelectMouseType2)
							{
							case 1:
								Cost.Mouse = 1000;
								break;
							case 2:
								Cost.Mouse = 1200;
								break;
							}
							break;
						}
						break;
					default:
						Cost.Mouse = 0;
						break;
					}
				}
				else
					Cost.Mouse = 0;

					
					//KeyBoard Section

				SetFocus(GetDlgItem(hDlg, ID_CBKEYBOARDCOM));
				iSelectKeyBoardCompany = SendMessage(hwndKeyBoardCompany, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectKeyBoardCompany != CB_ERR) && (iSelectKeyBoardCompany != 0))
				{
					ShowWindow(hwndKeyBoardType1, SW_SHOW);
				}

				if ((iSelectKeyBoardCompany != 0) && ((HWND)lParam == hwndKeyBoardCompany))
				{
					SetFocus(hwndKeyBoardType1);
					SendMessage(hwndKeyBoardType1, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; MOUSETYPE[i] != NULL; i++)
					{
						SendMessage(hwndKeyBoardType1, CB_ADDSTRING, WPARAM(0), LPARAM(KEYBOARDTYPE[i]));
					}
					SendMessage(hwndKeyBoardType1, CB_SETCURSEL, WPARAM(0), LPARAM(0));
					SendMessage(hwndKeyBoardType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				iSelectKeyBoardType1 = SendMessage(hwndKeyBoardType1, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectKeyBoardType1 != CB_ERR) && iSelectKeyBoardType1 != 0)
				{
					ShowWindow(hwndKeyBoardType2, SW_SHOW);
				}
				if ((iSelectKeyBoardType1 == 1) && (iSelectKeyBoardCompany == 1) && ((HWND)lParam == hwndKeyBoardType1))
				{
					SetFocus(hwndKeyBoardType2);
					SendMessage(hwndKeyBoardType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; KEYBOARDDELLWired[i] != NULL; i++)
					{
						SendMessage(hwndKeyBoardType2, CB_ADDSTRING, WPARAM(0), LPARAM(KEYBOARDDELLWired[i]));
					}
					SendMessage(hwndKeyBoardType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectKeyBoardType1 == 2) && (iSelectKeyBoardCompany == 1) && ((HWND)lParam == hwndKeyBoardType1))
				{
					SetFocus(hwndKeyBoardType2);
					SendMessage(hwndKeyBoardType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; KEYBOARDDELLWireLess[i] != NULL; i++)
					{
						SendMessage(hwndKeyBoardType2, CB_ADDSTRING, WPARAM(0), LPARAM(KEYBOARDDELLWireLess[i]));
					}
					SendMessage(hwndKeyBoardType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectKeyBoardType1 == 1) && (iSelectKeyBoardCompany == 2) && ((HWND)lParam == hwndKeyBoardType1))
				{
					SetFocus(hwndKeyBoardType2);
					SendMessage(hwndKeyBoardType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; KEYBOARDLOGITECHWired[i] != NULL; i++)
					{
						SendMessage(hwndKeyBoardType2, CB_ADDSTRING, WPARAM(0), LPARAM(KEYBOARDLOGITECHWired[i]));
					}
					SendMessage(hwndKeyBoardType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				if ((iSelectKeyBoardType1 == 2) && (iSelectKeyBoardCompany == 2) && ((HWND)lParam == hwndKeyBoardType1))
				{
					SetFocus(hwndKeyBoardType2);
					SendMessage(hwndKeyBoardType2, CB_RESETCONTENT, WPARAM(0), LPARAM(0));
					for (int i = 0; KEYBOARDLOGITECHWireLess[i] != NULL; i++)
					{
						SendMessage(hwndKeyBoardType2, CB_ADDSTRING, WPARAM(0), LPARAM(KEYBOARDLOGITECHWireLess[i]));
					}
					SendMessage(hwndKeyBoardType2, CB_SETCURSEL, WPARAM(0), LPARAM(0));
				}

				SetFocus(GetDlgItem(hDlg, ID_CBKEYBOARDSUBTYPE));
				iSelectKeyBoardType2 = SendMessage(hwndKeyBoardType2, CB_GETCURSEL, WPARAM(0), LPARAM(0));
				if ((iSelectKeyBoardCompany != 0) && (iSelectKeyBoardType1 != 0) && (iSelectKeyBoardType2 != 0))
				{
					switch (iSelectKeyBoardCompany)
					{
					case 1:
						switch (iSelectKeyBoardType1)
						{
						case 1:
							switch (iSelectKeyBoardType2)
							{
							case 1:
								Cost.Keyboard = 800;
								break;
							case 2:
								Cost.Keyboard = 1000;
							}
							break;

						case 2:
							switch (iSelectKeyBoardType2)
							{
							case 1:
								Cost.Keyboard = 2000;
								break;
							case 2:
								Cost.Keyboard = 2700;
								break;
							}
							break;
						}
						break;

					case 2:
						switch (iSelectKeyBoardType1)
						{
						case 1:
							switch (iSelectKeyBoardType2)
							{
							case 1:
								Cost.Keyboard = 600;
								break;
							case 2:
								Cost.Keyboard = 800;
							}
							break;

						case 2:
							switch (iSelectKeyBoardType2)
							{
							case 1:
								Cost.Keyboard = 1000;
								break;
							case 2:
								Cost.Keyboard = 1400;
								break;
							}
							break;
						}
						break;
					default:
						Cost.Keyboard = 0;
						break;
					}
				}
				else
					Cost.Keyboard = 0;

				SetFocus(GetDlgItem(hDlg,ID_CBMONITORCOM));
				iSelectMonitorCompany = SendMessage(hwndMonitorCompany,CB_GETCURSEL,WPARAM(0),LPARAM(0));
				if (iSelectMonitorCompany != CB_ERR)
				{
					switch (iSelectMonitorCompany)
					{
					case 0:
						Cost.Monitor = 0;
						break;
					case 1:
						Cost.Monitor = 5440;
						break;
					case 2:
						Cost.Monitor = 10434;
						break;
					case 3:
						Cost.Monitor = 15000;
						break;
					}
				}


				break;
//Ending of the First dialog box
		}
		return (TRUE);
	}
	return(FALSE);
	
}




BOOL CALLBACK MyDlgProc3(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	void CreateMyFile(TCHAR []);
	void WriteMyFile(TCHAR []);
	HDC hdc;
	PAINTSTRUCT ps;
	DWORD NumberOfBytesWritten = 0;
	TCHAR temp [25]= TEXT(".txt");
	TCHAR str1[125] = TEXT("\r\n----------------------------------------------------------------------------------------------------------\n\r");
	TCHAR Name[50];
	
//	TCHAR str2[125] = TEXT("\r\nPurchase-----------------Company--------------Size------------------Subtype----------------Subtype---------Cost\n\r");

	switch (iMsg)
	{
		case WM_PAINT:
			hdc =BeginPaint(hDlg,&ps);
			TextOut(hdc,60,70,TEXT("NAME"),4);
			EndPaint(hDlg,&ps);
			return(TRUE);

		case WM_COMMAND:
			switch (LOWORD(wParam))
			{
			case ID_OK:
				GetDlgItemText(hDlg,ID_NAME,Name,50);
				IsRKeyPressed = true;
				//MessageBox(NULL, Name, TEXT("info"),MB_OK);
				//hFile=CreateFile(Name,FILE_APPEND_DATA,0,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
				
				wcscat_s(Name,temp);
				if (wcscmp(Name , TEXT(".txt"))==0) 
					MessageBox(hDlg, TEXT("Please Enter Name"), TEXT("INFO"), MB_OK);
				else
				{
					CreateMyFile(Name);

					WriteMyFile(str1);
					WriteMyFile(TEXT("\nName: "));
					
					WriteMyFile(Name);
					WriteMyFile(str1);
				//	WriteMyFile(str2);

					if (Cost.Cpu != 0)
					{
						WriteMyFile(TEXT("\nCPU :"));
						WriteMyFile(TEXT("----------------------"));
						WriteMyFile(in.CpuType);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.CpuGen);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.CpuArch);
						WriteMyFile(TEXT("----------------->"));	
						WriteMyFile(Cost.CPUText);
						WriteMyFile(str1);

					}
					if (Cost.Ram != 0)
					{
						WriteMyFile(TEXT("\nRam :"));
						WriteMyFile(TEXT("-------------------->"));
						WriteMyFile(in.RamCompany);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(in.RamSize);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(Cost.RAMText);
						WriteMyFile(str1);

					}

					if (Cost.MotherBoard != 0)
					{
						WriteMyFile(TEXT("\nMotherBoard :"));
						WriteMyFile(TEXT("-------------------->"));
						WriteMyFile(in.MotherBoardCompany);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(in.MotherBoardType);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(Cost.MotherBoardText);
						WriteMyFile(str1);

					}

					if (Cost.GraphicsCard != 0)
					{
						WriteMyFile(TEXT("\n Graphics Card :"));
						WriteMyFile(TEXT("-------------------->"));
						WriteMyFile(in.GraphicsCardCompany);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(in.GraphicsCardType);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(Cost.GraphicsCardText);
						WriteMyFile(str1);

					}

					if (Cost.HardDisk != 0)
					{
						WriteMyFile(TEXT("\n Hard Disk :"));
						WriteMyFile(TEXT("-------------------->"));
						WriteMyFile(in.HardDiskCompany);
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(TEXT("1TB"));
						WriteMyFile(TEXT("-----------------"));
						WriteMyFile(Cost.HardDiskText);

						WriteMyFile(str1);

					}


					if (Cost.CdDvdDrive != 0)
					{
						WriteMyFile(TEXT("\n CD DVD Drive :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.CdDvdCompany);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.cdDvdType);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(Cost.CdDvdDriveText);

						WriteMyFile(str1);

					}


					if (Cost.Smps != 0)
					{
						WriteMyFile(TEXT("\n SMPS :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.SmpsCompany);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.SmpsType);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(Cost.SmpsText);
					}



					if (Cost.Cabinate != 0)
					{
						WriteMyFile(TEXT("\n Cabinate :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.CabinateCompany);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.CabinateType);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(Cost.CabinateText);

						WriteMyFile(str1);
					}

					if (Cost.Mouse != 0)
					{
						WriteMyFile(TEXT("\n Mouse :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.MouseCompany);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.MouseType1);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.MouseType2);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(Cost.MouseText);

						WriteMyFile(str1);
					}


					if (Cost.Keyboard != 0)
					{
						WriteMyFile(TEXT("\n Keyboard :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.KeyboardCompany);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.KeyBoardType1);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(in.KeyboardType2);
						WriteMyFile(TEXT("----------------->"));
						WriteMyFile(Cost.KeyboardText);

						WriteMyFile(str1);
					}

					if (Cost.Monitor != 0)
					{
						WriteMyFile(TEXT("\n Monitor :"));
						WriteMyFile(TEXT("--------------------"));
						WriteMyFile(in.MonitorCompany);
						WriteMyFile(TEXT("---------------->"));

						WriteMyFile(TEXT("---------------->"));
						//WriteMyFile(in.KeyboardType2);
						WriteMyFile(TEXT("---------------->"));
						WriteMyFile(Cost.MonitorText);

						WriteMyFile(str1);
					}


					WriteMyFile(TEXT("\n Total Cost: "));
					WriteMyFile(Cost.TotalCostText);


					WriteMyFile(str1);


					CloseHandle(hFile);
					MessageBox(hDlg,TEXT("File Created at Current Directory"),TEXT("Info"),MB_OK);
					InvalidateRect(ghwnd,NULL,TRUE);
					EndDialog(hDlg, 0);
				}
				
				
				return(TRUE);
			}
			return(TRUE);

	}

	return(FALSE);
}


void CreateMyFile(TCHAR FileName[])
{
	hFile = CreateFile(FileName, FILE_APPEND_DATA, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	
}

void WriteMyFile(TCHAR Data[])
{
	char *temp = (char*)malloc(wcslen(Data) * sizeof(TCHAR));
	
	WideCharToMultiByte(CP_UTF8, 0,LPCWSTR(Data),-1,temp, wcslen(Data) * sizeof(TCHAR),NULL,NULL );
	WriteFile(hFile, temp, strlen(temp), NULL, NULL);

}

BOOL CALLBACK MyDlgProc2(HWND hDlg, UINT iMsg, WPARAM wParam ,LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	HBRUSH hBrush = NULL;
	switch (iMsg)
	{

	case WM_PAINT:
		hdc=BeginPaint(hDlg,&ps);
		SetBkMode(hdc,TRANSPARENT);
		TextOut(hdc,100,75,TEXT("Project Done By: Tejaswini Balshetwar"),38);
		TextOut(hdc, 170, 90, TEXT("Thank You"),10);

		EndPaint(hDlg,&ps);
		return(TRUE);
	case WM_CTLCOLORDLG:
		hBrush = CreateSolidBrush(RGB(0, 255, 255));
		return((BOOL)hBrush);
		
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_OK:
			EndDialog(hDlg,0);
			return(TRUE);
		}
		return(TRUE);
	}
	return(FALSE);
}